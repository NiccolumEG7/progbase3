#include "dialog4choosing.h"
#include "ui_dialog4choosing.h"
#include "dialog4words.h"

Dialog4Choosing::Dialog4Choosing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog4Choosing)
{
    ui->setupUi(this);
}

Dialog4Choosing::~Dialog4Choosing()
{
    delete ui;
}

void Dialog4Choosing::on_pushButton_clicked()
{
    if (ui->automatic->isChecked()){
        //розпізнавання тут у майбутньому
    } else {
        Dialog4Words d4ws;
        d4ws.setModal(true);
        d4ws.exec();
    }
    this->hide();

}
