#ifndef DIALOGPRINT_H
#define DIALOGPRINT_H

#include <QDialog>
#include <QFile>

namespace Ui {
class DialogPrint;
}

class Dialog4Viewing : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog4Viewing(QWidget *parent = 0);
    ~Dialog4Viewing();

private slots:
    void displayWords();

    void displayTrans();

    void displayProns();

private:
    Ui::DialogPrint *ui;
};

#endif // DIALOGPRINT_H
