#ifndef DIALOG4TOTALERASE_H
#define DIALOG4TOTALERASE_H

#include <QDialog>
#include <QObject>
#include <QFile>

namespace Ui {
class Dialog4TotalErase;
}

class Dialog4Erasing : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog4Erasing(QWidget *parent = 0);
    ~Dialog4Erasing();

private slots:
    void on_buttonBox_accepted();

    void copyFile(QString, QString);

private:
    Ui::Dialog4TotalErase *ui;
};

#endif // DIALOG4TOTALERASE_H
