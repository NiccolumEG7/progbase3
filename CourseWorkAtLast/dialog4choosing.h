#ifndef DIALOG4CHOOSING_H
#define DIALOG4CHOOSING_H

#include <QDialog>

namespace Ui {
class Dialog4Choosing;
}

class Dialog4Choosing : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog4Choosing(QWidget *parent = 0);
    ~Dialog4Choosing();

private slots:

    void on_pushButton_clicked();

private:
    Ui::Dialog4Choosing *ui;
};

#endif // DIALOG4CHOOSING_H
