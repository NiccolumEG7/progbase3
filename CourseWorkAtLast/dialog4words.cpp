#include "dialog4words.h"
#include "ui_dialog4words.h"
#include "dialog4erasing.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QJsonArray>

Dialog4Words::Dialog4Words(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog4Words)
{
    ui->setupUi(this);
    ui->label_4->setVisible(false);
}

Dialog4Words::~Dialog4Words()
{
    delete ui;
}

void Dialog4Words::on_buttonBox_accepted()
{
    QString newWord = ui->lineEdit->text();
    if(ui->checkBox->isChecked()){
        QMessageBox::information(this, "Новий розділ", newWord);
    }

    addToFile("words.txt", newWord);

    Dialog4Words d4ws;
    d4ws.setModal(true);
    d4ws.displayWords();
    d4ws.exec();
    this->destroy();
}

void Dialog4Words::addToFile(QString filename, QString string){
    if (QString::compare(filename, "words.txt", Qt::CaseSensitive)){
        updateFile(filename, "copy.txt", string); //copy words to buffer & add a newbie word
        updateFile("copy.txt", filename, ""); //copy words back to the firstplase
    } else { //stings are equal -> we're working on words file
        updateFile(filename, "copyOfWords.txt", string);
        updateFile("copyOfWords.txt", filename, "");
    }
}

void Dialog4Words::updateFile(QString src, QString dest, QString newString){
    QFile read(src);
    QFile toWrite(dest);
    if(toWrite.open(QIODevice::WriteOnly) && read.open(QIODevice::ReadOnly)){
        QTextStream stream(&toWrite);
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            if (!line.trimmed().isEmpty())
                toWrite.write(line);
        }
        if (!newString.trimmed().isEmpty() && !newString.startsWith("#"))
            stream << ((ui->checkBox->isChecked())? "#" : "") + newString << endl;
        toWrite.close();
        read.close();
    }
    if(!(QString::compare(dest, "words.txt", Qt::CaseSensitive))) //stings are equal -> we're working on words file
        displayWords();
}


void Dialog4Words::displayWords(){
    ui->textBrowser->setText("");
    QFile read("words.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            ui->textBrowser->append(line);
        }
        read.close();
    }
}

void Dialog4Words::on_commandLinkButton_clicked()
{
    ui->label_4->show();
    Dialog4Erasing erase;
    erase.setModal(true);
    erase.exec();
    displayWords();
    ui->commandLinkButton->setChecked(false);
}


void Dialog4Words::translate(QString text, QString targetLanguage) {
    QNetworkAccessManager *networkManager = new QNetworkAccessManager();
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(handleTranslation(QNetworkReply*)));

    QUrl url("https://translation.googleapis.com/language/translate/v2?key=AIzaSyDohmb39vD7s0Mq73s5Yzui9X1OCwZAzVg");
    // Build your JSON string as usual
    QByteArray jsonString = this->getJsonForTranslateRequest(text, targetLanguage);
    // For your "Content-Length" header
    QByteArray postDataSize = QByteArray::number(jsonString.size());
    QNetworkRequest request(url);
    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("Content-Length", postDataSize);
    networkManager->post(request, jsonString);
}


void Dialog4Words::pronunciation(QString word) {
    QNetworkAccessManager *networkManager = new QNetworkAccessManager();
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(handlePronunciation(QNetworkReply*)));

    QUrl url("https://wordsapiv1.p.mashape.com/words");
    url.setPath(QString("%1/%2/pronunciation").arg(url.path()).arg(word));
    QNetworkRequest request(url);
    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("X-Mashape-Key", "cTXyM1fQ0umshGtMmUNPdkCxX075p1HXKMRjsnXIGEkM4fXXhy");
    networkManager->get(request);
}

QByteArray Dialog4Words::getJsonForTranslateRequest(QString text, QString targetLanguage) {
    //    {
    //      "q": "word",
    //      "source": null,
    //      "target": "uk",
    //      "format": "text"
    //    }
    QJsonObject request;
    request["q"] = text;
    request["target"] = targetLanguage;
    request["format"] = "text";
    QJsonDocument doc(request);
    return doc.toJson();
}

void Dialog4Words::handleTranslation(QNetworkReply* reply) {
    QJsonObject response = QJsonDocument::fromJson(((QString) reply->readAll()).toUtf8()).object();
    addToFile("translations.txt", (response["data"].toObject()["translations"].toArray()[0].toObject()["translatedText"]).toString());
}

void Dialog4Words::handlePronunciation(QNetworkReply* reply) {
    QJsonObject response = QJsonDocument::fromJson(((QString) reply->readAll()).toUtf8()).object();
    addToFile("pronunciations.txt", (response["pronunciation"].toObject()["all"]).toString());
}

void Dialog4Words::translateWords(QString lang){
    eraseFile("translations.txt");
    QFile read("words.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            if (!line.trimmed().isEmpty()){
                if (!line.startsWith('#')){
                    translate(line, lang);
                }else{
                    addToFile("translations.txt", line.remove(0, 1));
                }
            }
        }
        read.close();
    }
}

void Dialog4Words::pronunciationWords(){
    eraseFile("pronunciations.txt");
    QFile read("words.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            if (!line.trimmed().isEmpty()){
                if (!line.startsWith('#')){
                    pronunciation(line.trimmed());
                }else{
                    addToFile("pronunciations.txt", line.remove(0, 1));
                }
            }
        }
        read.close();
    }
}

void Dialog4Words::on_pushButton_3_clicked()
{
    updateFile("copyOfWords.txt", "words.txt", "");
}

void Dialog4Words::on_pushButton_2_clicked()
{
    updateFile("blank.txt", "words.txt", "");
    updateFile("blank.txt", "translations.txt", "");
    updateFile("blank.txt", "pronunciations.txt", "");
}

void Dialog4Words::on_pushButton_clicked()
{
    translateWords("uk");
    pronunciationWords();
}

void Dialog4Words::_manualErase4SetUp()
{
    eraseFile("words.txt");
    eraseFile("translations.txt");
    eraseFile("pronunciations.txt");
}

void Dialog4Words::copyFile(QString from, QString to){
    updateFile(from, to, "");
}

void Dialog4Words::eraseFile(QString fileName){
    copyFile("blank.txt", fileName);
}


