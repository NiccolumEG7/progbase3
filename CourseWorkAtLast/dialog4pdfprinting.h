#ifndef DIALOG4PDFPRINTING_H
#define DIALOG4PDFPRINTING_H

#include <QDialog>

namespace Ui {
class Dialog4pdfPrinting;
}

class Dialog4pdfPrinting : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog4pdfPrinting(bool pdfPrint = true, QWidget *parent = 0);
    ~Dialog4pdfPrinting();

private slots:
    void on_pushButton_clicked();
    void pdf(QString, bool, int, QString);
private:
    Ui::Dialog4pdfPrinting *ui;
};

#endif // DIALOG4PDFPRINTING_H
