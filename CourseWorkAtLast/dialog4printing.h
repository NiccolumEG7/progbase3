#ifndef DIALOG4PRINTING_H
#define DIALOG4PRINTING_H

#include <QDialog>

namespace Ui {
class Dialog4Printing;
}

class Dialog4Printing : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog4Printing(QWidget *parent = 0);
    ~Dialog4Printing();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Dialog4Printing *ui;
};

#endif // DIALOG4PRINTING_H
