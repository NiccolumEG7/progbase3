#ifndef DIALOG4WORDS_H
#define DIALOG4WORDS_H

#include <QDialog>
#include <QObject>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QNetworkAccessManager>
#include <QNetworkRequest>

using namespace std;

namespace Ui {
class Dialog4Words;
}

class Dialog4Words : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog4Words(QWidget *parent = 0);
    ~Dialog4Words();

    void _manualErase4SetUp();

    void translateWords(QString);

    void pronunciationWords();

private slots:
    void on_buttonBox_accepted();

    void displayWords();

    void updateFile(QString, QString, QString);

    void addToFile(QString, QString);

    void copyFile(QString, QString);

    void eraseFile(QString);

    void on_commandLinkButton_clicked();

    void handleTranslation(QNetworkReply*);

    void handlePronunciation(QNetworkReply*);

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::Dialog4Words *ui;

    void translate(QString, QString);

    void pronunciation(QString);

    QByteArray getJsonForTranslateRequest(QString, QString);
};



#endif // DIALOG4WORDS_H
