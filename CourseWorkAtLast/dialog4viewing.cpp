#include "dialog4viewing.h"
#include "ui_dialogprint.h"

Dialog4Viewing::Dialog4Viewing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPrint)
{
    ui->setupUi(this);
    displayWords();
    displayTrans();
    displayProns();
}

Dialog4Viewing::~Dialog4Viewing()
{
    delete ui;
}

void Dialog4Viewing::displayWords(){
    ui->textBrowser->setText("");
    QFile read("words.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            ui->textBrowser->append(line);
        }
        read.close();
    }
}
void Dialog4Viewing::displayTrans(){
    ui->textBrowser_2->setText("");
    QFile read("translations.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            ui->textBrowser_2->append(line);
        }
        read.close();
    }
}
void Dialog4Viewing::displayProns(){
    ui->textBrowser_3->setText("");
    QFile read("pronunciations.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            ui->textBrowser_3->append(line);
        }
        read.close();
    }
}
