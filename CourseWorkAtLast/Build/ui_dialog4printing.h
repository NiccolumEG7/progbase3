/********************************************************************************
** Form generated from reading UI file 'dialog4printing.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG4PRINTING_H
#define UI_DIALOG4PRINTING_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog4Printing
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QLabel *label;
    QPushButton *pushButton;

    void setupUi(QDialog *Dialog4Printing)
    {
        if (Dialog4Printing->objectName().isEmpty())
            Dialog4Printing->setObjectName(QStringLiteral("Dialog4Printing"));
        Dialog4Printing->resize(357, 310);
        gridLayoutWidget = new QWidget(Dialog4Printing);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 331, 281));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        radioButton = new QRadioButton(gridLayoutWidget);
        radioButton->setObjectName(QStringLiteral("radioButton"));

        gridLayout->addWidget(radioButton, 1, 0, 1, 1);

        radioButton_2 = new QRadioButton(gridLayoutWidget);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));

        gridLayout->addWidget(radioButton_2, 2, 0, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        pushButton = new QPushButton(gridLayoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 3, 0, 1, 1);


        retranslateUi(Dialog4Printing);

        QMetaObject::connectSlotsByName(Dialog4Printing);
    } // setupUi

    void retranslateUi(QDialog *Dialog4Printing)
    {
        Dialog4Printing->setWindowTitle(QApplication::translate("Dialog4Printing", "\320\241\320\277\320\276\321\201\321\226\320\261 \320\264\321\200\321\203\320\272\321\203", 0));
        radioButton->setText(QApplication::translate("Dialog4Printing", "\320\243 .pdf \321\204\320\260\320\271\320\273", 0));
        radioButton_2->setText(QApplication::translate("Dialog4Printing", "\320\236\320\264\321\200\320\260\320\267\321\203 \320\275\320\260 \320\277\321\200\320\270\320\275\321\202\320\265\321\200", 0));
        label->setText(QApplication::translate("Dialog4Printing", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">\320\222\320\270\320\261\320\265\321\200\321\226\321\202\321\214 \321\201\320\277\320\276\321\201\321\226\320\261 \320\264\321\200\321\203\320\272\321\203</span></p></body></html>", 0));
        pushButton->setText(QApplication::translate("Dialog4Printing", "\320\224\320\260\320\273\321\226", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog4Printing: public Ui_Dialog4Printing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG4PRINTING_H
