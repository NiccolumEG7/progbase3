/********************************************************************************
** Form generated from reading UI file 'dialog4choosing.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG4CHOOSING_H
#define UI_DIALOG4CHOOSING_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog4Choosing
{
public:
    QWidget *formLayoutWidget;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QRadioButton *automatic;
    QRadioButton *handMode;
    QPushButton *pushButton;

    void setupUi(QDialog *Dialog4Choosing)
    {
        if (Dialog4Choosing->objectName().isEmpty())
            Dialog4Choosing->setObjectName(QStringLiteral("Dialog4Choosing"));
        Dialog4Choosing->resize(341, 141);
        Dialog4Choosing->setMinimumSize(QSize(0, 0));
        Dialog4Choosing->setMaximumSize(QSize(341, 312));
        formLayoutWidget = new QWidget(Dialog4Choosing);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 316, 121));
        gridLayout = new QGridLayout(formLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        groupBox = new QGroupBox(formLayoutWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        automatic = new QRadioButton(groupBox);
        automatic->setObjectName(QStringLiteral("automatic"));

        verticalLayout->addWidget(automatic);

        handMode = new QRadioButton(groupBox);
        handMode->setObjectName(QStringLiteral("handMode"));

        verticalLayout->addWidget(handMode);

        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);


        gridLayout->addWidget(groupBox, 1, 0, 1, 1);


        retranslateUi(Dialog4Choosing);

        QMetaObject::connectSlotsByName(Dialog4Choosing);
    } // setupUi

    void retranslateUi(QDialog *Dialog4Choosing)
    {
        Dialog4Choosing->setWindowTitle(QApplication::translate("Dialog4Choosing", "\320\222\320\270\320\261\321\226\321\200 \321\200\320\265\320\266\320\270\320\274\321\203 \320\262\320\262\320\276\320\264\321\203 \321\201\320\273\321\226\320\262", 0));
        groupBox->setTitle(QApplication::translate("Dialog4Choosing", "\320\222\320\270\320\261\320\265\321\200\321\226\321\202\321\214 \321\200\320\265\320\266\320\270\320\274 \320\262\320\262\320\276\320\264\321\203 \321\201\320\273\321\226\320\262", 0));
        automatic->setText(QApplication::translate("Dialog4Choosing", "\320\260\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\275\320\265\321\202 \321\200\320\276\320\267\320\277\321\226\320\267\320\275\320\260\320\262\320\275\320\275\321\217 (\320\267 \321\204\320\276\321\202\320\276)", 0));
        handMode->setText(QApplication::translate("Dialog4Choosing", "\321\200\321\203\321\207\320\275\320\265 \320\262\320\262\320\265\320\264\320\265\320\275\320\275\321\217 (\320\275\320\260\320\261\321\226\321\200 \320\267 \320\272\320\273\320\260\320\262\320\270)", 0));
        pushButton->setText(QApplication::translate("Dialog4Choosing", "\320\223\320\276\321\202\320\276\320\262\320\276", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog4Choosing: public Ui_Dialog4Choosing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG4CHOOSING_H
