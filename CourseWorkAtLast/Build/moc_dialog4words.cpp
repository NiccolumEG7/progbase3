/****************************************************************************
** Meta object code from reading C++ file 'dialog4words.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dialog4words.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialog4words.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Dialog4Words_t {
    QByteArrayData data[15];
    char stringdata0[241];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Dialog4Words_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Dialog4Words_t qt_meta_stringdata_Dialog4Words = {
    {
QT_MOC_LITERAL(0, 0, 12), // "Dialog4Words"
QT_MOC_LITERAL(1, 13, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 12), // "displayWords"
QT_MOC_LITERAL(4, 49, 10), // "updateFile"
QT_MOC_LITERAL(5, 60, 9), // "addToFile"
QT_MOC_LITERAL(6, 70, 8), // "copyFile"
QT_MOC_LITERAL(7, 79, 9), // "eraseFile"
QT_MOC_LITERAL(8, 89, 28), // "on_commandLinkButton_clicked"
QT_MOC_LITERAL(9, 118, 17), // "handleTranslation"
QT_MOC_LITERAL(10, 136, 14), // "QNetworkReply*"
QT_MOC_LITERAL(11, 151, 19), // "handlePronunciation"
QT_MOC_LITERAL(12, 171, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(13, 195, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(14, 219, 21) // "on_pushButton_clicked"

    },
    "Dialog4Words\0on_buttonBox_accepted\0\0"
    "displayWords\0updateFile\0addToFile\0"
    "copyFile\0eraseFile\0on_commandLinkButton_clicked\0"
    "handleTranslation\0QNetworkReply*\0"
    "handlePronunciation\0on_pushButton_3_clicked\0"
    "on_pushButton_2_clicked\0on_pushButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Dialog4Words[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x08 /* Private */,
       3,    0,   75,    2, 0x08 /* Private */,
       4,    3,   76,    2, 0x08 /* Private */,
       5,    2,   83,    2, 0x08 /* Private */,
       6,    2,   88,    2, 0x08 /* Private */,
       7,    1,   93,    2, 0x08 /* Private */,
       8,    0,   96,    2, 0x08 /* Private */,
       9,    1,   97,    2, 0x08 /* Private */,
      11,    1,  100,    2, 0x08 /* Private */,
      12,    0,  103,    2, 0x08 /* Private */,
      13,    0,  104,    2, 0x08 /* Private */,
      14,    0,  105,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,    2,    2,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    2,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    2,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Dialog4Words::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Dialog4Words *_t = static_cast<Dialog4Words *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_buttonBox_accepted(); break;
        case 1: _t->displayWords(); break;
        case 2: _t->updateFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 3: _t->addToFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->copyFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 5: _t->eraseFile((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->on_commandLinkButton_clicked(); break;
        case 7: _t->handleTranslation((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 8: _t->handlePronunciation((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 9: _t->on_pushButton_3_clicked(); break;
        case 10: _t->on_pushButton_2_clicked(); break;
        case 11: _t->on_pushButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject Dialog4Words::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Dialog4Words.data,
      qt_meta_data_Dialog4Words,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Dialog4Words::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Dialog4Words::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Dialog4Words.stringdata0))
        return static_cast<void*>(const_cast< Dialog4Words*>(this));
    return QDialog::qt_metacast(_clname);
}

int Dialog4Words::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
