/********************************************************************************
** Form generated from reading UI file 'dialog4words.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG4WORDS_H
#define UI_DIALOG4WORDS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog4Words
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QTextBrowser *textBrowser;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_4;
    QCommandLinkButton *commandLinkButton;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QCheckBox *checkBox;
    QDialogButtonBox *buttonBox;
    QGridLayout *gridLayout_3;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer;
    QLabel *label_5;

    void setupUi(QDialog *Dialog4Words)
    {
        if (Dialog4Words->objectName().isEmpty())
            Dialog4Words->setObjectName(QStringLiteral("Dialog4Words"));
        Dialog4Words->resize(570, 401);
        Dialog4Words->setMinimumSize(QSize(570, 401));
        gridLayoutWidget = new QWidget(Dialog4Words);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(20, 10, 531, 371));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        pushButton_3 = new QPushButton(gridLayoutWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        gridLayout_2->addWidget(pushButton_3, 0, 1, 1, 1);

        pushButton_2 = new QPushButton(gridLayoutWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        gridLayout_2->addWidget(pushButton_2, 0, 0, 1, 1);


        gridLayout->addLayout(gridLayout_2, 1, 0, 1, 1);

        groupBox = new QGroupBox(gridLayoutWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMaximumSize(QSize(280, 290));
        gridLayout_4 = new QGridLayout(groupBox);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        scrollArea = new QScrollArea(groupBox);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setMaximumSize(QSize(260, 230));
        scrollArea->setLineWidth(0);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 242, 228));
        textBrowser = new QTextBrowser(scrollAreaWidgetContents);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 241, 231));
        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_4->addWidget(scrollArea, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setEnabled(true);

        verticalLayout_2->addWidget(label_4);

        commandLinkButton = new QCommandLinkButton(gridLayoutWidget);
        commandLinkButton->setObjectName(QStringLiteral("commandLinkButton"));
        QFont font;
        font.setPointSize(15);
        commandLinkButton->setFont(font);

        verticalLayout_2->addWidget(commandLinkButton);


        gridLayout->addLayout(verticalLayout_2, 1, 1, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font);

        verticalLayout->addWidget(label);

        lineEdit = new QLineEdit(gridLayoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        checkBox = new QCheckBox(gridLayoutWidget);
        checkBox->setObjectName(QStringLiteral("checkBox"));

        verticalLayout->addWidget(checkBox);

        buttonBox = new QDialogButtonBox(gridLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        pushButton = new QPushButton(gridLayoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout_3->addWidget(pushButton, 1, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_2, 1, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 1, 0, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_3->addWidget(label_5, 0, 1, 1, 1);


        verticalLayout->addLayout(gridLayout_3);


        gridLayout->addLayout(verticalLayout, 0, 1, 1, 1);


        retranslateUi(Dialog4Words);
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog4Words, SLOT(reject()));
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog4Words, SLOT(accept()));

        QMetaObject::connectSlotsByName(Dialog4Words);
    } // setupUi

    void retranslateUi(QDialog *Dialog4Words)
    {
        Dialog4Words->setWindowTitle(QApplication::translate("Dialog4Words", "\320\244\320\276\321\200\320\274\321\203\320\262\320\260\320\275\320\275\321\217 \321\201\320\273\320\276\320\262\320\275\320\270\320\272\320\260", 0));
        pushButton_3->setText(QApplication::translate("Dialog4Words", "\320\222\321\226\320\264\320\275\320\276\320\262\320\270\321\202\320\270 \321\227\321\205*", 0));
        pushButton_2->setText(QApplication::translate("Dialog4Words", "\320\222\320\270\320\264\320\260\320\273\320\270\321\202\320\270 \321\206\321\226 \321\201\320\273\320\276\320\262\320\260", 0));
        groupBox->setTitle(QApplication::translate("Dialog4Words", "          \320\222\320\260\321\210 \321\201\320\273\320\276\320\262\320\275\320\270\320\272", 0));
        textBrowser->setHtml(QApplication::translate("Dialog4Words", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\320\242\321\203\321\202 \320\262\321\226\320\264\320\276\320\261\321\200\320\260\320\266\320\260\321\202\320\270\320\274\321\203\321\202\321\214\321\201\321\217 \320\262\320\262\320\265\320\264\320\265\320\275\321\226 [\320\260\320\275\320\263\320\273\321\226\320\271\321\201\321\214\320\272\321\226] \321\201\320\273\320\276\320\262\320\260</p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"justify\" s"
                        "tyle=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">#[Title] - \320\267\320\260\320\263\320\276\320\273\320\276\320\262\320\272\320\270, \321\211\320\276 \320\275\320\265 \320\277\320\265\321\200\320\265\320\272\320\273\320\260\320\264\320\260\321\202\320\270\320\274\321\203\321\202\321\214\321\201\321\217</p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\320\241\320\270\320\274\320\262\320\276\320\273 '#' \320\275\320\260 \320\277\320\276\321\207\320\260\321\202\320\272\321\203 \321\201\320\273\320\276\320\262\320\260 &quot;\320\267\320\260\321\200\320\265\320\267\320\265\321\200\320\262\320\276\320\262\320\260\320\275\320\270\320\271&quot; </p>\n"
"<p align=\"justify\" sty"
                        "le=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- \320\267 \320\275\321\214\320\276\320\263\320\276 \320\275\320\265 \320\274\320\276\320\266\320\275\320\260 \320\277\320\276\321\207\320\270\320\275\320\260\321\202\320\270 \321\201\320\273\320\276\320\262\320\260</p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">*\320\241\320\273\320\276\320\262\320\260 \320\274\320\276\320\266\320\275\320\260 \320\262\321\226\320\264\320\275\320\276\320\262\320\270\321\202\320\270, \321\217\320\272\321\211\320\276 \320\262\320\270 \320\275\320\265 \320\275\320\260\321\202\320\270\321\201\320\272\320\260\320\273\320\270 &quot;\320\236\321\207\320\270\321\201\320\270\321\202\321\202\320\270 "
                        "\320\262\321\201\321\226 \320\264\320\260\320\275\321\226&quot; \321\202\320\260 \320\264\320\276 \321\202\320\276\320\263\320\276 \321\217\320\272 \320\262\320\270 \320\264\320\276\320\261\320\260\320\262\320\270\321\202\320\265 \320\275\320\276\320\262\320\265 \321\201\320\273\320\276\320\262\320\276</p></body></html>", 0));
        label_4->setText(QApplication::translate("Dialog4Words", "<html><head/><body><p align=\"center\"><span style=\" font-size:9pt; color:#ff0000;\">(\320\262\320\276\321\200\320\276\321\202\321\202\321\217 \320\275\320\260\320\267\320\260\320\264 \320\275\320\265\320\274\320\260)</span></p></body></html>", 0));
        commandLinkButton->setText(QApplication::translate("Dialog4Words", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\320\270 \320\262\321\201\321\226 \320\264\320\260\320\275\321\226", 0));
        label->setText(QApplication::translate("Dialog4Words", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">\320\224\320\276\320\261\320\260\320\262\321\202\320\265 \321\201\320\273\320\276\320\262\320\276</span></p></body></html>", 0));
        lineEdit->setText(QString());
        checkBox->setText(QApplication::translate("Dialog4Words", "\320\267\320\260\320\263\320\276\320\273\320\276\320\262\320\276\320\272 \321\200\320\276\320\267\320\264\321\226\320\273\321\203", 0));
        pushButton->setText(QApplication::translate("Dialog4Words", "\320\237\320\265\321\200\320\265\320\272\320\273\320\260\321\201\321\202\320\270", 0));
        label_5->setText(QApplication::translate("Dialog4Words", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; font-weight:600;\">\320\223\320\276\321\202\320\276\320\262\320\276?</span></p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog4Words: public Ui_Dialog4Words {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG4WORDS_H
