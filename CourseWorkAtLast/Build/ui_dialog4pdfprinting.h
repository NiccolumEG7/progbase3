/********************************************************************************
** Form generated from reading UI file 'dialog4pdfprinting.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG4PDFPRINTING_H
#define UI_DIALOG4PDFPRINTING_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog4pdfPrinting
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *verticalSpacer;
    QLabel *label;
    QSpacerItem *verticalSpacer_3;
    QPushButton *pushButton;
    QHBoxLayout *horizontalLayout_8;
    QCheckBox *checkBox;
    QLineEdit *lineEdit_2;
    QSlider *horizontalSlider;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_5;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QLabel *label_6;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_4;

    void setupUi(QDialog *Dialog4pdfPrinting)
    {
        if (Dialog4pdfPrinting->objectName().isEmpty())
            Dialog4pdfPrinting->setObjectName(QStringLiteral("Dialog4pdfPrinting"));
        Dialog4pdfPrinting->resize(362, 261);
        Dialog4pdfPrinting->setMinimumSize(QSize(362, 261));
        Dialog4pdfPrinting->setMaximumSize(QSize(362, 261));
        gridLayoutWidget = new QWidget(Dialog4pdfPrinting);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 341, 232));
        gridLayout_2 = new QGridLayout(gridLayoutWidget);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_5->addItem(verticalSpacer);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_5->addWidget(label);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_5->addItem(verticalSpacer_3);


        gridLayout_2->addLayout(horizontalLayout_5, 1, 0, 3, 1);

        pushButton = new QPushButton(gridLayoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout_2->addWidget(pushButton, 7, 0, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        checkBox = new QCheckBox(gridLayoutWidget);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setAutoFillBackground(false);
        checkBox->setChecked(true);

        horizontalLayout_8->addWidget(checkBox);

        lineEdit_2 = new QLineEdit(gridLayoutWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        horizontalLayout_8->addWidget(lineEdit_2);


        gridLayout_2->addLayout(horizontalLayout_8, 6, 0, 1, 1);

        horizontalSlider = new QSlider(gridLayoutWidget);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(horizontalSlider, 4, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_6->addItem(verticalSpacer_2);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_6->addWidget(label_5);

        lineEdit = new QLineEdit(gridLayoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_6->addWidget(lineEdit);


        gridLayout_2->addLayout(horizontalLayout_6, 0, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_4->addWidget(label_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_4->addWidget(label_6);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(40, 20));

        horizontalLayout_4->addWidget(label_3);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);


        gridLayout_2->addLayout(horizontalLayout_4, 5, 0, 1, 1);


        retranslateUi(Dialog4pdfPrinting);

        QMetaObject::connectSlotsByName(Dialog4pdfPrinting);
    } // setupUi

    void retranslateUi(QDialog *Dialog4pdfPrinting)
    {
        Dialog4pdfPrinting->setWindowTitle(QApplication::translate("Dialog4pdfPrinting", "\320\234\320\260\320\271\320\266\320\265 \320\264\321\200\321\203\320\272\321\203\321\224\320\274\320\276...", 0));
        label->setText(QApplication::translate("Dialog4pdfPrinting", "<html><head/><body><p align=\"center\">\320\222\320\270\320\261\320\265\321\200\321\226\321\202\321\214 \321\202\320\276\320\262\321\211\320\270\320\275\321\203 \322\221\321\200\320\260\321\202\320\272\320\270 \321\202\320\260\320\261\320\273\320\270\321\206\321\226</p></body></html>", 0));
        pushButton->setText(QApplication::translate("Dialog4pdfPrinting", "\320\237\320\276\321\227\321\205\320\260\320\273\320\270!", 0));
        checkBox->setText(QApplication::translate("Dialog4pdfPrinting", "\320\224\320\276\320\264\320\260\321\202\320\270 \320\275\320\260\320\264\320\267\320\260\320\263\320\276\320\273\320\276\320\262\320\276\320\272:", 0));
        lineEdit_2->setText(QApplication::translate("Dialog4pdfPrinting", "\320\241\320\273\320\276\320\262\320\275\320\270\320\272", 0));
        label_5->setText(QApplication::translate("Dialog4pdfPrinting", "\320\235\320\260\320\267\320\262\320\260 \321\204\320\260\320\271\320\273\321\203:", 0));
        lineEdit->setText(QApplication::translate("Dialog4pdfPrinting", "example.pdf", 0));
        label_2->setText(QApplication::translate("Dialog4pdfPrinting", "<html><head/><body><p align=\"right\">0</p></body></html>", 0));
        label_6->setText(QApplication::translate("Dialog4pdfPrinting", "4", 0));
        label_3->setText(QApplication::translate("Dialog4pdfPrinting", "<html><head/><body><p align=\"right\">5</p></body></html>", 0));
        label_4->setText(QApplication::translate("Dialog4pdfPrinting", "<html><head/><body><p>9</p></body></html>", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog4pdfPrinting: public Ui_Dialog4pdfPrinting {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG4PDFPRINTING_H
