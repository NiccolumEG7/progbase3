/********************************************************************************
** Form generated from reading UI file 'dialog4totalerase.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG4TOTALERASE_H
#define UI_DIALOG4TOTALERASE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog4TotalErase
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *Dialog4TotalErase)
    {
        if (Dialog4TotalErase->objectName().isEmpty())
            Dialog4TotalErase->setObjectName(QStringLiteral("Dialog4TotalErase"));
        Dialog4TotalErase->resize(233, 121);
        gridLayoutWidget = new QWidget(Dialog4TotalErase);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 231, 121));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        buttonBox = new QDialogButtonBox(gridLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Vertical);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);


        retranslateUi(Dialog4TotalErase);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog4TotalErase, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog4TotalErase, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog4TotalErase);
    } // setupUi

    void retranslateUi(QDialog *Dialog4TotalErase)
    {
        Dialog4TotalErase->setWindowTitle(QApplication::translate("Dialog4TotalErase", "\320\222\320\230\320\224\320\220\320\233\320\230\320\242\320\230 \320\222\320\241\320\225?", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog4TotalErase: public Ui_Dialog4TotalErase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG4TOTALERASE_H
