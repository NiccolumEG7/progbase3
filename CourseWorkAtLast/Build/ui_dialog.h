/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_Dialog4Choosing
{
public:

    void setupUi(QDialog *Dialog4Choosing)
    {
        if (Dialog4Choosing->objectName().isEmpty())
            Dialog4Choosing->setObjectName(QStringLiteral("Dialog4Choosing"));
        Dialog4Choosing->resize(240, 320);

        retranslateUi(Dialog4Choosing);

        QMetaObject::connectSlotsByName(Dialog4Choosing);
    } // setupUi

    void retranslateUi(QDialog *Dialog4Choosing)
    {
        Dialog4Choosing->setWindowTitle(QApplication::translate("Dialog4Choosing", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog4Choosing: public Ui_Dialog4Choosing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
