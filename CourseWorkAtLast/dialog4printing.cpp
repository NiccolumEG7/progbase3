#include "dialog4printing.h"
#include "ui_dialog4printing.h"
#include "dialog4pdfprinting.h"

Dialog4Printing::Dialog4Printing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog4Printing)
{
    ui->setupUi(this);
}

Dialog4Printing::~Dialog4Printing()
{
    delete ui;
}

void Dialog4Printing::on_pushButton_clicked()
{
    Dialog4pdfPrinting d4pdf(!ui->radioButton_2->isChecked());
    d4pdf.setModal(true);
    d4pdf.exec();
}
