#include "dialog4pdfprinting.h"
#include "ui_dialog4pdfprinting.h"
#include <QApplication>
#include <QtCore>
#include <QPrinter>
#include <QPdfWriter>
#include <QPainter>
#include <QTextCharFormat>
#include <QTextDocument>
#include <QTextCursor>
#include <QMessageBox>
#include <QDesktopServices>
#include <QPrintDialog>

bool pdfPrintMethod = true;

QString form_HTML_Row(bool isTitle, int id, QString word, QString pronunciation, QString translation, int titlesCount = 0){
    QString row;
    if (isTitle){
        row =
                "            <TR ALIGN=\"CENTER\">"
                "               <TD ALIGN=\"CENTER\">" + QString::number(id - titlesCount + 1) + "</TD>"
                "               <TD ALIGN=\"CENTER\">" + word + "</TD>"
                "               <TD ALIGN=\"CENTER\">" + pronunciation + "</TD>"
                "               <TD ALIGN=\"CENTER\">" + translation + "</TD>"
                "            </TR>";
    } else {
        row = "<TR>"
                            "<TH COLSPAN=\"4\"><BR>" + word.mid(1) +
                            "</TH>"
                      "</TR>";
    }
    return row;
}

QString form_HTML_Table(QStringList words, QStringList prons, QStringList trans){
    QString result = "";
    int titlesCount = 0;
    for (int i = 0; i < words.length(); ++i){
        bool isTitle = !words[i].startsWith('#');
        if (isTitle){
            ++titlesCount;
        }
        result += form_HTML_Row(isTitle, i, words[i], prons[i], trans[i], titlesCount);
    }
    return result;
}

QStringList getWords(){
    QStringList words;
    QFile read("words.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            if (!line.trimmed().isEmpty()){
                words.append(line);
            }
        }
        read.close();
    }
    return words;
}

QStringList getProns(){
    QStringList prons;
    QFile read("pronunciations.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            if (!line.trimmed().isEmpty()){
                prons.append(line);
            }
        }
        read.close();
    }
    return prons;
}

QStringList getTrans(){
    QStringList trans;
    QFile read("translations.txt");
    if(read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            if (!line.trimmed().isEmpty()){
                trans.append(line);
            }
        }
        read.close();
    }
    return trans;
}

void Dialog4pdfPrinting::pdf(QString filename, bool hasTitle, int  borderWidth, QString title = "Словник")
{
    ////тестові строки
    //(проконали) = worked nicely
//    QString words[] = {"#travel", "landscape", "travel", "#miracles", "miracle", "marvel"};
//    QString prons[] = {"_", "l@NDskap'", "TrEveL", "_", "m1rEcl\'", "m@rvEl"};
//    QString trans[] = {"_", "ландшафт", "подорож", "_", "мед_Микола здав сесію", "ого!"};
    QString headerTitle = "";
    if (hasTitle)
        headerTitle += "<TR>"
                            "<TH COLSPAN=\"4\"><BR><H3>" + title + "</H3>"
                            "</TH>"
                       "</TR>";

    QString html =
         "<TABLE BORDER=\"" + QString::number(borderWidth) +
            "\"    WIDTH=\"100%\"   CELLPADDING=\"9\" CELLSPACING=\"3\">"
            + headerTitle +
"            <TR>"
"               <TH>№</TH>"
"               <TH>Слово</TH>"
"               <TH>Транскрипція</TH>"
"               <TH>Переклад</TH>"
"            </TR>"
+ form_HTML_Table(getWords(), getProns(), getTrans()) +
"         </TABLE>";

    QTextDocument document;
    document.setHtml(html);
    if(pdfPrintMethod){
    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName(filename);
    printer.setPageMargins(QMarginsF(15, 15, 15, 15));
    document.print(&printer);
    } else {
    QPrintDialog pd(this);
    QPrinter *pr;
    pd.exec();
    pr = pd.printer();
    document.print(pr);
    }
}

Dialog4pdfPrinting::Dialog4pdfPrinting(bool pdfPrint, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog4pdfPrinting)
{
    ui->setupUi(this);
    ui->lineEdit->setDisabled(!(pdfPrintMethod = pdfPrint));
    ui->horizontalSlider->setRange(0, 9);
}

Dialog4pdfPrinting::~Dialog4pdfPrinting()
{
    delete ui;
}

void Dialog4pdfPrinting::on_pushButton_clicked()
{
    QString filename = ui->lineEdit->text();
    int borderWidth = ui->horizontalSlider->value();
    bool hasTitle = ui->checkBox->isChecked();
    QString title = ui->lineEdit_2->text();
    pdf(filename, hasTitle, borderWidth, title);
    QMessageBox::information(this, "Готово!", "Ви ввели:\n ім'я файлу: " + filename +
                             "\n товщина таблиці: " + QString::number(borderWidth) + "\n "
                             + ((!hasTitle)?"немає заголовку":("заголовок: " + title)));
    ///тут відкривається створений нами файл за адресою папки з білдом та вказаною юзером назвою
    //пам-па-ра-па-пам \\todo: видалити цю строку з комментом. Строка: "бувайте! я - спати"
    QDesktopServices::openUrl(QUrl("file:///home/mykola-invictus/QtStuff/CourseWorkAtLast/Build/" + filename));

}
