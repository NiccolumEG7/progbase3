#include "dialog4erasing.h"
#include "ui_dialog4totalerase.h"

Dialog4Erasing::Dialog4Erasing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog4TotalErase)
{
    ui->setupUi(this);
}

Dialog4Erasing::~Dialog4Erasing()
{
    delete ui;
}

void Dialog4Erasing::on_buttonBox_accepted()
{
    copyFile("blank.txt", "words.txt");
    copyFile("blank.txt", "copyOfWords.txt");
    copyFile("blank.txt", "translations.txt");
    copyFile("blank.txt", "pronunciations.txt");
}

void Dialog4Erasing::copyFile(QString src, QString dest){
    QFile read(src);
    QFile toWrite(dest);
    if(toWrite.open(QIODevice::WriteOnly) && read.open(QIODevice::ReadOnly)){
        while (!read.atEnd()) {
            QByteArray line = read.readLine();
            if (!line.trimmed().isEmpty())
                toWrite.write(line);
        }
        toWrite.close();
        read.close();
    }
}
