#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog4viewing.h"
#include "dialog4choosing.h"
#include "dialog4printing.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QWidget::setWindowIcon(QIcon(QString("myapp.svg")));
    ui->setupUi(this);
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    Dialog4Choosing d4c;
    d4c.setModal(true);
    d4c.exec();
}

void MainWindow::on_pushButton_3_clicked()
{
    Dialog4Viewing view;
    view.setModal(true);
    view.exec();
}

void MainWindow::on_pushButton_2_clicked()
{
    Dialog4Printing d4p;
    d4p.setModal(true);
    d4p.exec();
}
