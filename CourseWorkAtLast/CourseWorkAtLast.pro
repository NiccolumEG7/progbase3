#-------------------------------------------------
#
# Project created by QtCreator 2017-06-22T00:48:58
#
#-------------------------------------------------

QT       += core gui network printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CourseWorkAtLast
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog4words.cpp \
    dialog4choosing.cpp \
    dialog4viewing.cpp \
    dialog4erasing.cpp \
    dialog4printing.cpp \
    dialog4pdfprinting.cpp

HEADERS  += mainwindow.h \
    dialog4words.h \
    dialog4choosing.h \
    dialog4viewing.h \
    dialog4erasing.h \
    dialog4printing.h \
    dialog4pdfprinting.h

FORMS    += mainwindow.ui \
    dialog4words.ui \
    dialog4choosing.ui \
    dialog4viewing.ui \
    dialog4erasing.ui \
    dialog4printing.ui \
    dialog4pdfprinting.ui

CONFIG += c++11
